CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Module Details
 * Dependencies
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

Mobisms module is an integration with rules module which provides free SMS
service to users in India using mobisms API.

MODULE DETAILS
--------------

This module uses an unofficial mobisms API
(https://github.com/kingster/MobiSMS-API) in which it requires a sender's
phone number which should be registered at mobisms
(http://site24.mobisms.com/) before using this module.
The same phone number and password is used to trigger the API. The
receiver's phone number(may not be registered at mobisms) and message
is configurable and can be used as per the requirements.

DEPENDENCIES
------------

* Libraries (https://www.drupal.org/project/libraries)
  This is a dependency for way2SMS inorder to check the way2SMS API.

* Rules (https://www.drupal.org/project/rules)
  This is a dependency for way2SMS as the way2SMS API is triggered with
  event, SMS action.

INSTALLATION
-------------

* Download the way2SMS module.
* Download way2SMS API from (https://github.com/kingster/MobiSMS-API)
  and place it in libraries/mobisms
  such that mobisms-api.php is available as
  libraries/mobisms/mobisms-api.php.
* Enable the module and configure login credentials(Admin Mobile number
  and password).

CONFIGURATIONS
--------------
* Configure sender's (admin) phone number and password at
  /admin/config/services/mobisms.
* Create Rule as per your requirement and add mobisms action:
  Send SMS, for the event you want the SMS to be sent.

TROUBLESHOOTING
---------------

MobiSMS works only on the correct Sender's phone number and password and
the API correctly downloaded and placed in libraries directory.
In case the module is not working, please check :
* /admin/config/services/mobisms - for correct credentials.
* mobisms libraries are not placed correctly as per described in installation.
* Check the action for which you are triggering the SMS service.
* Clear cache with change in rules.
* Reinstall the module after disable and uninstallation.

MAINTAINERS
-----------

Current maintainers:

 * Kajal Kiran (https://www.drupal.org/u/kajalkiran)
 * Anand Toshniwal (https://www.drupal.org/u/anandtoshniwal93)

This project has been sponsored by:
 * QED42
  QED42 is a web development agency focussed on helping organisations and
  individuals reach their potential, most of our work is in the space of
  publishing, e-commerce, social and enterprise.
