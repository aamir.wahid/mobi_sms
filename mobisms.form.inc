<?php

/**
 * @file
 * MobiSMS Configuration form settings.
 */

/**
 * Configuration form to set credentials.
 */
function mobisms_form($form, &$form_state) {

  $form['mobisms_senders_phone_number'] = [
    '#type' => 'textfield',
    '#title' => t("Sender's contact number"),
    '#description' => t('Enter the registered contact number at mobisms. For example: 7722800915. Note: Do not use +91, 0.'),
    '#default_value' => variable_get('mobisms_senders_phone_number'),
    '#element_validate' => ['element_validate_number'],
    '#size' => 10,
    '#maxlength' => 10,
    '#required' => TRUE,
  ];
  $form['mobisms_senders_password'] = [
    '#type' => 'password',
    '#title' => t('MobiSMS account password'),
    '#description' => t('Enter login password as your mobisms password. Note: You need to have an account to mobisms.'),
    '#size' => 10,
    '#required' => TRUE,
  ];

  $form['#validate'][] = '_mobisms_form_validate_admin_credentials';

  return system_settings_form($form);
}

/**
 * Validate Entered Credentials.
 */
function _mobisms_form_validate_admin_credentials($form, &$form_state) {
  if ($form_state['values']['mobisms_senders_phone_number'] && $form_state['values']['mobisms_senders_password']) {
    if (!_mobisms_login($form_state['values']['mobisms_senders_phone_number'], $form_state['values']['mobisms_senders_password'])) {
      form_set_error('mobisms_senders_phone_number', t('Please provide valid Credentials'));
    }
  }
}
