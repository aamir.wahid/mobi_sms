<?php

namespace Drupal\mobisms\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class MobismsSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mobisms_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mobisms.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('mobisms.settings');
    $form['mobisms_senders_phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Sender's User ID"),
      '#description' => $this->t('Enter the registered contact number at Mobishastra. For example: 7722800915. Note: Do not use +91, 0.'),
      '#default_value' => $config->get('mobisms_senders_phone_number'),
      '#size' => 10,
      '#maxlength' => 10,
      '#required' => TRUE,
    ];
    $form['mobisms_senders_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Mobishastra account password'),
      '#description' => $this->t('Enter login password as your MobiSMS password. Note: You need to have an account to mobisms.'),
      '#size' => 10,
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['mobisms_senders_phone_number'] && $values['mobisms_senders_password']) {
      if (!_mobisms_login($values['mobisms_senders_phone_number'], $values['mobisms_senders_password'])) {
        drupal_set_message('mobisms_senders_phone_number', t('Please provide valid Credentials'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('mobisms.settings')
      ->set('mobisms_senders_phone_number', $values['mobisms_senders_phone_number'])
      ->set('mobisms_senders_password', $values['mobisms_senders_password'])
      ->save();
  }

}
